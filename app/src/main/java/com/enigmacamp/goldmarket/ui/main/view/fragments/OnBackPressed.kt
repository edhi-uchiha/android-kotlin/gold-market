package com.enigmacamp.goldmarket.ui.main.view.fragments

interface OnBackPressed {
    fun onBackPressed()
}