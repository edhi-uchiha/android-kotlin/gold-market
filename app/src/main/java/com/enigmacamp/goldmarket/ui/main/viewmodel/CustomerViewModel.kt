package com.enigmacamp.goldmarket.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.enigmacamp.goldmarket.data.customer.CustomerRepository
import com.enigmacamp.goldmarket.data.customer.Customers

class CustomerViewModel() : ViewModel() {

    lateinit var customer: LiveData<Customers>
    lateinit var repo: CustomerRepository

    constructor(customerRepository: CustomerRepository, id: Int) : this() {
        repo = customerRepository
        customer = customerRepository.getCustomer(id)
    }

}