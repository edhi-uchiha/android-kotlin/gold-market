package com.enigmacamp.goldmarket.ui.main.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.enigmacamp.goldmarket.data.repository.*

object SignInViewModelInjector {
    private fun provideUserAuthRepo(app: Context): UserAuthRepository {
        return UserAuthRepository.instance.apply {
            var dataStore = UserAuthDataStoreImpl()
            dataStore.init(app)
            init(dataStore)
        }
    }

    private fun provideCustomerBalanceRepo(): CustomerBalanceRepository {
        return CustomerBalanceRepository.instance.apply {
            init(CustomerBalanceDataStoreImpl())
        }
    }

    fun getFactory(app: Context): ViewModelProvider.Factory {
        return SignInViewModelFactory(provideUserAuthRepo(app), provideCustomerBalanceRepo())
    }
}