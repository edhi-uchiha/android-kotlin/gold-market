package com.enigmacamp.goldmarket.data.transactions

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Transactions(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "type") val type : String,
    @ColumnInfo(name = "customer_id") val customerId : Int,
    @ColumnInfo(name = "gold_amount") val goldAmount: Double,
    @ColumnInfo(name = "price") val price: Int,
    @ColumnInfo(name = "total_price") val totalPrice: Int,
): Parcelable