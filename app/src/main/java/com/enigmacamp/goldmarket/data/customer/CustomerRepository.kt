package com.enigmacamp.goldmarket.data.customer

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.enigmacamp.goldmarket.data.repository.UserAuthDataStore
import com.enigmacamp.goldmarket.data.repository.UserAuthDataStoreImpl

class CustomerRepository(app: Context) {

    private var customerDao: UserAuthDataStore

    var customer: LiveData<Customers> = MutableLiveData<Customers>()

    init {

        customerDao = UserAuthDataStoreImpl()
        customerDao.init(app)
    }

    fun getCustomer(id: Int): LiveData<Customers> {
        customer = customerDao.getCustomer(id)
        return customer
    }
}
