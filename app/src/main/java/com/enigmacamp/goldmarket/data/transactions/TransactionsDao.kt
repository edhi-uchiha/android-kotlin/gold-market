package com.enigmacamp.goldmarket.data.transactions

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TransactionsDao {

    @Query("SELECT * FROM transactions")
    fun getAll(): List<Transactions>

    @Query("SELECT * FROM transactions WHERE customer_id IN (:customerId)")
    fun findTransactionByCustomerId(customerId: Int): Transactions

    @Query("SELECT * FROM transactions WHERE id = :id")
    fun findById(id: String): Transactions

    @Insert
    fun insert(transactions: Transactions)

    @Delete
    fun delete(transactions: Transactions)

}