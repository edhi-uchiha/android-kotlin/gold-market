package com.enigmacamp.goldmarket.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.enigmacamp.goldmarket.data.customer.CustomerDao
import com.enigmacamp.goldmarket.data.customer.Customers
import com.enigmacamp.goldmarket.data.model.Customer
import com.enigmacamp.goldmarket.data.room.AppDatabase

class UserAuthDataStoreImpl() : UserAuthDataStore {

    private lateinit var database: AppDatabase
    private lateinit var dao: CustomerDao

    override fun init(app: Context) {
        database = AppDatabase.getDatabase(app)
        dao = database.customerDao()
    }

    override fun getUserAuth(email: String, password: String): Customer? {

        val customer = dao.findByEmail(email)
        return if(customer != null && customer.password == password) {
            Customer(customer.id.toString(), customer.firstName!!, customer.lastName!!, customer.email!!)
        } else {
            null
        }
    }

    override fun registerUser(customer: Customers) {
        dao.insert(customer)
    }

    override fun getAllUser(): List<Customers> {
        return dao.getAll()
    }

    override fun getById(id: Int): Customers {
        return  dao.getById(id)
    }

    override fun update(customer: Customers) {
        dao.update(customer)
    }

    override fun getCustomer(id: Int): LiveData<Customers> {
       return dao.getCustomer(id)
    }
}