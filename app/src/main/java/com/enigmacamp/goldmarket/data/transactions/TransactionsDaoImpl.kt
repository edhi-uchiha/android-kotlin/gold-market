package com.enigmacamp.goldmarket.data.transactions

import com.enigmacamp.goldmarket.data.room.AppDatabase

class TransactionsDaoImpl(database: AppDatabase) : TransactionsDao {

    var appDatabase: AppDatabase = database
    var trxDao = appDatabase.transactionsDao()
    var cusDao = appDatabase.customerDao()

    override fun getAll(): List<Transactions> {
        return trxDao.getAll()
    }

    override fun findTransactionByCustomerId(customerId: Int): Transactions {
        return trxDao.findTransactionByCustomerId(customerId)
    }

    override fun findById(id: String): Transactions {
        return trxDao.findById(id)
    }

    override fun insert(transactions: Transactions) {
        var customer = cusDao.getById(transactions.customerId)
        if(transactions.type == "BELI"){
            customer.goldAmount = customer.goldAmount?.plus(transactions.goldAmount)
        } else {
            customer.goldAmount = customer.goldAmount?.minus(transactions.goldAmount)
        }
        cusDao.update(customer)
        return trxDao.insert(transactions)
    }

    override fun delete(transactions: Transactions) {
        return trxDao.delete(transactions)
    }
}