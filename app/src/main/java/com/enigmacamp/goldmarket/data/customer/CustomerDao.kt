package com.enigmacamp.goldmarket.data.customer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*

@Dao
interface CustomerDao {

    @Query("SELECT * FROM customers")
    fun getAll(): List<Customers>

    @Query("SELECT * FROM customers WHERE id IN (:customerId)")
    fun getById(customerId: Int): Customers

    @Query("SELECT * FROM customers WHERE email = :email LIMIT 1")
    fun findByEmail(email: String): Customers

    @Query("SELECT * FROM customers WHERE id IN (:customerId)")
    fun getCustomer(customerId: Int): LiveData<Customers>

    @Insert
    fun insert(customer: Customers)

    @Update
    fun update(customer: Customers)

    @Delete
    fun delete(customer: Customers)
}