package com.enigmacamp.goldmarket.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.enigmacamp.goldmarket.data.customer.CustomerDao
import com.enigmacamp.goldmarket.data.customer.Customers
import com.enigmacamp.goldmarket.data.transactions.Transactions
import com.enigmacamp.goldmarket.data.transactions.TransactionsDao

@Database(entities = [Customers::class, Transactions::class], version = 4)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        @Volatile
        private var INSTANCE : AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "gold_market"
                )
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                instance
            }
        }
    }

    abstract fun customerDao(): CustomerDao
    abstract fun transactionsDao() : TransactionsDao

}