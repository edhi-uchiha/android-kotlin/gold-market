package com.enigmacamp.goldmarket.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.enigmacamp.goldmarket.data.customer.Customers
import com.enigmacamp.goldmarket.data.model.Customer

interface UserAuthDataStore {
     fun init(app: Context)
     fun getUserAuth(email: String, password: String): Customer?
     fun registerUser(customer: Customers)
     fun getAllUser(): List<Customers>
     fun getById(id: Int): Customers
     fun update(customer: Customers)
     fun getCustomer(id: Int) : LiveData<Customers>
}