package com.enigmacamp.goldmarket.data.repository

import com.enigmacamp.goldmarket.data.model.CustomerBalance

class CustomerBalanceDataStoreImpl : CustomerBalanceDataStore {
    override fun getBalance(id: String): CustomerBalance? {
        return if (id == "1") {
            CustomerBalance("1", 100)
        } else {
            null
        }
    }
}